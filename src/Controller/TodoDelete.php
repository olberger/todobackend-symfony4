<?php
// src/Controller/TodoDelete.php

namespace App\Controller;

use App\Repository\TodoRepository;

use App\Entity\Todo;

/**
 * Custom operation handler for DELETE on collections 
 * see https://api-platform.com/docs/core/operations#recommended-method
 */
class TodoDelete
{
    /**
     * @var TodoRepository used for deleting all items
     */
    private $entityRepository;

    public function __construct(TodoRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke()
    {
        if( $this->entityRepository->deleteAll() )
        {
            return array();
        }
    }
}
