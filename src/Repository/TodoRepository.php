<?php

namespace App\Repository;

use App\Entity\Todo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Custom repository used for adding the means to delete all todo items
 *
 */
class TodoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Todo::class);
    }

    /**
     * Delete all instances of Todo
     * 
     * @return mixed|\Doctrine\DBAL\Driver\Statement|array|NULL
     */
    public function deleteAll()
    {
        $isDeleted = $this->createQueryBuilder("todo")
            ->delete()
            ->getQuery()->execute();

        return $isDeleted;
    }
}
