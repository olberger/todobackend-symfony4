<?php
// src/Serializer/ApiNormalizer
namespace App\Serializer;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Add url property to the Todo items
 *
 * see https://api-platform.com/docs/core/content-negotiation#writing-a-custom-normalizer
 * 
 * Note that we don't apply instructions at https://api-platform.com/docs/core/serialization/#decorating-a-serializer-and-add-extra-data
 * since we care for JSON and not JSON-LD here
 */
final class ApiNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{

    private $decorated;

    /**
     * @var Router injected to generate the URL from the path
     */
    private $router;

    public function __construct(NormalizerInterface $decorated, Router $router)
    {
        if (! $decorated instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException(sprintf('The decorated normalizer must implement the %s.', DenormalizerInterface::class));
        }
        
        $this->decorated = $decorated;
        
        $this->router = $router;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $data = $this->decorated->normalize($object, $format, $context);
        if (is_array($data)) {
            $url = $this->router->generate('api_todos_get_item', [
                'id' => $object->getId()
            ], UrlGeneratorInterface::ABSOLUTE_URL);
            $data['url'] = $url;
        }
        
        return $data;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $this->decorated->supportsDenormalization($data, $type, $format);
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return $this->decorated->denormalize($data, $class, $format, $context);
    }
    
    public function setSerializer(SerializerInterface $serializer)
    {
        if($this->decorated instanceof SerializerAwareInterface) {
            $this->decorated->setSerializer($serializer);
        }
    }
}
